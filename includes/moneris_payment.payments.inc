<?php
/**
 * @file
 * Contains all of the forms and functions to be included when processing payments.
 * Only loaded when needed by the commerce module.
 */

 /**
  * Returns the default configurations for the Moneris Settings form.
  */
function _moneris_payment_default_settings() {
  return array(
    'txn_mode' => MONERIS_TXN_MODE_DEVELOPER,
    'moneris_api_version' => '',
    'moneris_production' => array(
      'store_id' => '',
      'apitoken' => '',
    ),
    'moneris_testing' => array(
      'store_id' => 'store1',
      'apitoken' => 'yesguy',
      'test_order_prefix' => '',
    ),
    'moneris_features' => array(
      'retain'  => 0,
      'options' => array(),
    ),
  );
}

 /**
  * Returns an array of possible security options.
  */
function _moneris_payment_security_options() { 
  return array
  (
     'type'    => t('Card Type'),
     'owner'   => t('Card Holders Name'),
     'address' => t('Address Verification (AVS)'),
     'code'    => t('CVV verification'),
  );
}
 
/**
 * Menu callback; Displays the administration settings for Moneris Payment.
 */
function _moneris_payment_settings_form($settings = NULL) {
  // Merge default settings into the stored settings array.
  $settings = (array) $settings + _moneris_payment_default_settings();
 
  $form = array();

  $form['txn_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Live or test settings'),
    '#options' => array(
      MONERIS_TXN_MODE_LIVE => t('Live transactions'),
      MONERIS_TXN_MODE_DEVELOPER => t('Moneris test server transactions'),
      MONERIS_TXN_MODE_LIVE_TEST => t('Live test transactions'),
    ),
    '#default_value' => $settings['txn_mode'],
    '#description' => t('Adjust to Live transactions when you are ready to start processing real payments.') . '<br />' . t('Test account transactions go through Moneris\' test server and will use the <i>Testing Settings</i> below. Live test transactions are live transactions with real money using your <i>production settings</i>, but you can override the $ amount at checkout. This allows you to test without having to create test products with low prices, instead you can use any product and just change the amount your account will be charged.'),
  );

  $form['moneris_api_version'] = array(
    '#type' => 'select',
    '#title' => t('API Version'),
    '#description' => t('The Moneris API differs slightly between the US and Canadian versions.'),
    '#default_value' => $settings['moneris_api_version'],
    '#options' => array(
      '' => t('Canada'),
      'us_' => t('United States'),
    ),
  );
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 50,
  );
 $form['moneris_production'] = array(
    '#type' => 'fieldset',
    '#title' => t('Production Settings'),
    '#group' => 'settings',
    '#description' => t('Add your production store details here'),
  );
 $form['moneris_production']['store_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Store ID'),
    '#default_value' => $settings['moneris_production']['store_id'],
  );
  $form['moneris_production']['apitoken'] = array(
    '#type' => 'textfield',
    '#title' => t('API TOKEN'),
    '#default_value' => $settings['moneris_production']['apitoken'],
  );

  $form['moneris_testing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testing Settings'),
    '#group' => 'settings',
    '#description' => t('Add your test store details here'),
  );
 $form['moneris_testing']['store_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Store ID'),
    '#default_value' => $settings['moneris_testing']['store_id'],
  );
  $form['moneris_testing']['apitoken'] = array(
    '#type' => 'textfield',
    '#title' => t('Test API TOKEN'),
    '#default_value' => $settings['moneris_testing']['apitoken'],
  );
  $form['moneris_testing']['test_order_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Order Prefix'),
    '#default_value' => $settings['moneris_testing']['test_order_prefix'],
    '#description' => t('Since many people may be using the same Moneris test credentials you need unique order ids to determine which are your own. This field will be used as a prefix to the order id. A 6 character random hash will also be used so your test order ids will be of the form: yourprefix_randhash_orderid, i.e. test123_83j38d_12'),
  );
  $form['moneris_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credit Card Processing Features'),
    '#group' => 'settings',
    '#description' => t('Select the options that you have enabled/purchased with your Moneris merchant account.'),
  );
 $form['moneris_features']['options'] = array(
    '#type' => 'checkboxes',
    '#options'         => _moneris_payment_security_options(),
    '#title' => t('Additional Security Options'),
    '#default_value' => $settings['moneris_features']['options'],
  );
  
  return system_settings_form($form);
}

/**
 * Payment method callback: checkout form.
 */
function _moneris_payment_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  $defaults = array(
    'type' => '',
    'owner' => '',
    'number' => '',
    'start_month' => '',
    'start_year' => '',
    'exp_month' => '',
    'exp_year' => '',
    'issue' => '',
    'code' => '',
    'bank' => '',
    'tprice' => '1.00',
  );

  $security_opts = _moneris_payment_security_options();
  foreach ($payment_method['settings']['moneris_features']['options'] as $opt => $opt_title) {
    if (isset($security_opts[$opt]) && !$opt_title) unset($security_opts[$opt]);
  }
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $form = commerce_payment_credit_card_form($security_opts, $defaults);

  $testing_message = '';
  switch ($payment_method['settings']['txn_mode']) {
    case MONERIS_TXN_MODE_DEVELOPER:
    //development environment
      $testing_message = t('Moneris: You are currently using your test environment details');
      // Merge in test default values.
      $form['credit_card']['number']['#type'] = 'select';
      $form['credit_card']['number']['#description'] = 'select';
      $form['credit_card']['number']['#options'] = array( 
        '5454545454545454' => '5454545454545454 (MASTERCARD)',
        '4242424242424242' => '4242424242424242 (VISA)',
        '4502285070000007' => '4502285070000007 (VISA always approved)',
        '373599005095005'  => '373599005095005 (AMEX)',
        '36462462742008'   => '36462462742008 (DINERS)'
      );
      unset($form['credit_card']['number']['#size']);
    case MONERIS_TXN_MODE_LIVE_TEST:
    //development and live test environment
      $testing_message = t('Moneris: You are currently using a <strong>live test environment</strong>. All transactions are real, but you can override the amount by entering a <i>test price value</i>.');
      $form['tprice'] = array(
        '#type' => 'textfield',
        '#title' => t('Test price value'),
        '#description' => t("This is the value that will be submitted to Moneris' test environment. Test Response depends on cent value: 0.00 - 10.00, *.00 Approved in all cases. Check Simulator penny value codes documentation provided by Moneris."),
        '#default_value' => $defaults['tprice'],
        '#required' => TRUE,
      );
      break;
  }
  if ($testing_message) {
    drupal_set_message($testing_message, 'warning');
  }

  return $form;
}

/**
 * Payment method callback: checkout form validation.
 */
function _moneris_payment_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  // Validate the credit card fields.
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}


/**
 * Payment method callback: checkout form submission.
 */
function _moneris_payment_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  return _moneris_payment_transaction($payment_method, $order, $charge, $pane_values);
}

/**
 * Creates a moneris payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $name
 *   The name entered on the submission form.
 */
function _moneris_payment_transaction($payment_method, $order, $charge, $values) {
  require_once(drupal_get_path('module', 'moneris_payment') . '/api/mpgClasses.php');

  /************************ Request Variables **********************************/
  switch ($payment_method['settings']['txn_mode']) {
    case MONERIS_TXN_MODE_LIVE_TEST:
    case MONERIS_TXN_MODE_LIVE:
      $orderid='order-' . $order->order_id . '-' . time();
      $store_id=$payment_method['settings']['moneris_production']['store_id'];
      $api_token=$payment_method['settings']['moneris_production']['apitoken'];
      break;
    case MONERIS_TXN_MODE_DEVELOPER:
      //Make sure we're using Moneris' test server for test transactions.
      $init_globs = new mpgGlobals(array('MONERIS_HOST' => 'esqa.moneris.com'));

      $orderid=$payment_method['settings']['moneris_testing']['test_order_prefix'] . '-' . substr( md5('yesguy' . date('U')), 0, 6 ) . '-' . $order->order_id;
      $store_id=$payment_method['settings']['moneris_testing']['store_id'];
      $api_token=$payment_method['settings']['moneris_testing']['apitoken'];
      break;
  }
  
  if (isset($values['tprice'])) {
    //override the amount to charge the card
    $amount=number_format($values['tprice'], 2, '.', '');
  }
  else {
    $amount=number_format(commerce_currency_amount_to_decimal($charge['amount'], $charge['currency_code']), 2, '.', '');
  }
  
  /************************ Transaction Variables ******************************/
  $pan=$values['credit_card']['number'];
  $expiry_date=substr($values['credit_card']['exp_year'], 2, 2) . $values['credit_card']['exp_month'];
  //Set the customer id.
  if (!empty($order->uid)) {// Use the user id if available
    $cust_id = 'cust-user-id-' . $order->uid;
  } else if (!empty($order->mail)) {//Otherwise use the email address if available.
    $cust_id = 'cust-email-' . $order->mail;
  } else {//All else fails use the order id.
    $cust_id = 'cust-order-id-' . $order->id;
  }
  $crypt = '7';
  
/******************** Customer Information Object *********************/
  $mpgCustInfo = new mpgCustInfo();
/********************** Set Customer Information **********************/
/*//TODO: Come up with a configurable way to map customer profile info
  //      to mpgCustInfo parameters. For now just add line item info.
  //      The following commented-out code comes from Moneris' integration
  //      guide.

  $billing = array(
    'first_name' => $first_name,
    'last_name' => $last_name,
    'company_name' => $company_name,
    'address' => $address,
    'city' => $city,
    'province' => $province,
    'postal_code' => $postal_code,
    'country' => $country,
    'phone_number' => $phone_number,
    'fax' => $fax,
    'tax1' => $tax1,
    'tax2' => $tax2,
    'tax3' => $tax3,
    'shipping_cost' => $shipping_cost
  );
  $mpgCustInfo->setBilling($billing);
  $shipping = array(
    'first_name' => $first_name,
    'last_name' => $last_name,
    'company_name' => $company_name,
    'address' => $address,
    'city' => $city,
    'province' => $province,
    'postal_code' => $postal_code,
    'country' => $country,
    'phone_number' => $phone_number,
    'fax' => $fax,
    'tax1' => $tax1,
    'tax2' => $tax2,
    'tax3' => $tax3,
    'shipping_cost' => $shipping_cost
  );
  $mpgCustInfo->setShipping($shipping);
  $mpgCustInfo->setEmail($email);
  $mpgCustInfo->setInstructions($instructions);

/*********************** Set Line Item Information *********************/
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $items = array();
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $mpgCustInfo->setItems( array(
           'name'            => $line_item_wrapper->line_item_label->value(),
           'quantity'        => round($line_item_wrapper->quantity->value(), 2),
           'product_code'    => '',
           'extended_amount' => ''
         )
    );
    }
  }

/***************** Transactional Associative Array ********************/
  $txnArray=array(
    'type' => $payment_method['settings']['moneris_api_version'] . 'purchase',
    'order_id' => "$orderid",
    'cust_id' => "$cust_id",
    'amount' => $amount,
    'pan' => $pan,
    'expdate' => $expiry_date,
    'crypt_type' => $crypt,
//    commcard_tax_amount => '0.15',
    'dynamic_descriptor' => 'online sale'
  );


/********************** Transaction Object ****************************/
  $mpgTxn = new mpgTransaction($txnArray);
/******************** Set Customer Information ************************/
//$mpgTxn->setCustInfo($mpgCustInfo);
/************************* Request Object *****************************/
  $mpgRequest = new mpgRequest($mpgTxn);
/************************ HTTPS Post Object ***************************/
  $mpgHttpPost =new mpgHttpsPost($store_id, $api_token, $mpgRequest);
/****************8********** Response *********************************/

  $mpgResponse=$mpgHttpPost->getMpgResponse();
  
  $transaction = commerce_payment_transaction_new('moneris_payment', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->remote_id = $mpgResponse->responseData['TransID'];
  $transaction->payload[REQUEST_TIME] = $mpgResponse->responseData;
  $transaction->remote_status = $mpgResponse->responseData['ResponseCode'];
  
  $transaction->message = 'Moneris ResponseCode: @responsecode <br />Moneris ResponseMessage: @message';
  $transaction->message_variables = array(
    '@responsecode' => $mpgResponse->responseData['ResponseCode'],
    '@message'      => $mpgResponse->responseData['Message']
  );
  $tsuccess = _moneris_payment_process_response ($mpgResponse);
  if ($tsuccess) {
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  }
  else {
    // If the payment failed, display an error and rebuild the form.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    drupal_set_message(t('We received the following error processing your card. Please enter you information again or try a different card.'), 'error');
    drupal_set_message(check_plain($mpgResponse->moneris_payment_failure_message), 'error');
  }

  commerce_payment_transaction_save($transaction);
  if (!$tsuccess) {
    return FALSE;
  }
}

 /**
  * Process the response from Moneris and decide what actions and message should result.
  */
function _moneris_payment_process_response(&$mpgResponse) {
  if (!$mpgResponse || !$mpgResponse->responseData) {
  //The mpgResponse variable is not valid.
    watchdog('moneris_payment', 'Serious Moneris transaction problem: Null gateway response.', array(), WATCHDOG_CRITICAL);
    $mpgResponse->moneris_payment_failure_message = '';
    return FALSE;
  }
  if (($completed = $mpgResponse->getComplete()) && ('true' === strtolower($completed))) {
  //We have a successful transaction which could be approved or declined. Lets take a look.
    $responseCode = (int)$mpgResponse->getResponseCode();
    if ($responseCode < 50) {
      //APPROVED
      return TRUE;
    }
    else {
      //DECLINED
      $mpgResponse->moneris_payment_failure_message = '';
      switch ($responseCode) {
        case 51:
        case 482:
        case 484:
        case 901:
         $mpgResponse->moneris_payment_failure_message = t('Expired Card');
         break;
        case 55:
        case 56:
        case 70:
        case 71:
        case 73:
        case 75:
        case 252:
         $mpgResponse->moneris_payment_failure_message = t('Card Not Supported | Not Approved');
         break;
        case 57:
        case 58:
        case 59:
        case 72:
        case 89:
        case 909:
         $mpgResponse->moneris_payment_failure_message = t('Card Use has been Limited, check with your card company');
         break;
        case 74:
        case 113:
        case 212:
        case 810:
         $mpgResponse->moneris_payment_failure_message = t('Connection problem with card issuer, please try again');
         break;
        case 76:
        case 95:
        case 907:
        case 908:
         $mpgResponse->moneris_payment_failure_message = t('Insufficient funds');
         break;
        case 77:
         $mpgResponse->moneris_payment_failure_message = t('$ Limit exceeded');
         break;
        case 78:
         $mpgResponse->moneris_payment_failure_message = t('Duplicate transaction');
         watchdog('moneris_payment', 'Transaction declined because of duplication: !param', array('!param' => '<pre>' . check_plain(print_r($mpgResponse->responseData, TRUE)) . '</pre>'), WATCHDOG_WARNING);
         break;
        case 107:
         $mpgResponse->moneris_payment_failure_message = t('Over daily limit');
         break;
        case 110:
         $mpgResponse->moneris_payment_failure_message = t('Usage Exceeded');
         break;
        case 475:
         $mpgResponse->moneris_payment_failure_message = t('Invalid Expiry Date');
         break;
        case 486:
        case 487:
        case 489:
        case 490:
         $mpgResponse->moneris_payment_failure_message = t('Invalid Expiry Date');
         break;
        case 960:
         watchdog('moneris_payment', 'Initialization failure - merchant number mismatch: !param', array('!param' => '<pre>' . check_plain(print_r($mpgResponse->responseData, TRUE)) . '</pre>'), WATCHDOG_WARNING);
        default:
        //We at least know that it was declined.
        $mpgResponse->moneris_payment_failure_message = 'Declined: ' . $mpgResponse->getMessage();
      }
      return FALSE;
    }
  }
  else {
    if (!$completed) {
    //Something went really wrong we should never have this case.
      watchdog('moneris_payment', 'Serious Moneris transaction problem: Invalid $mpgResponse->getComplete() data.', array(), WATCHDOG_CRITICAL);
      $mpgResponse->moneris_payment_failure_message = t('Connection issue, please notify the store if this problem persists');
    } elseif ($timeout = $mpgResponse->getTimedOut()) {
      if ('true' === strtolower($timeout)) {
      //Timeout!
        $mpgResponse->moneris_payment_failure_message = t('Transaction Timeout');
      } elseif ('false' === strtolower($timeout)) {
      //Most likely a problem involving the merchant account
        watchdog('moneris_payment', 'Potential merchant account issue: !param', array( '!param' => '<pre>' . check_plain(print_r($mpgResponse->responseData, TRUE)) . '</pre>'), WATCHDOG_ERROR);
        $mpgResponse->moneris_payment_failure_message = t('Merchant issue, please notify the store if this problem persists');
      } elseif ('null' === strtolower($timeout)) {
      //Most likely a connection issue
        watchdog('moneris_payment', 'Potential network|connection issue, verify that you can browse to !testlink if you are testing or !livelink if live: !param', array('!testlink'=>l('esqa.moneris.com','https://esqa.moneris.com'), '!testlink'=>l('www3.moneris.com','https://www3.moneris.com'), '!param' => '<pre>' . check_plain(print_r($mpgResponse->responseData, TRUE)) . '</pre>'), WATCHDOG_ERROR);
        $mpgResponse->moneris_payment_failure_message = t('Merchant network issue, please notify the store if this problem persists');
      }
      else {
      //Should never get this case.
        watchdog('moneris_payment', 'Serious Moneris transaction problem: Unknown $mpgResponse->getComplete() value returned: !param', array('!param' => '<pre>' . check_plain(print_r($mpgResponse->responseData, TRUE)) . '</pre>'), WATCHDOG_CRITICAL);
        $mpgResponse->moneris_payment_failure_message = 'Connection issue, please notify the store if this problem persists';
      }
    }
    else {
    //Again.. should never get this case.
        watchdog('moneris_payment', 'Serious Moneris transaction problem: Invalid $mpgResponse->getTimedOut() data.', array(), WATCHDOG_CRITICAL);
        $mpgResponse->moneris_payment_failure_message = t('Connection issue, please notify the store if this problem persists');
    }
  }
  return FALSE;
}
